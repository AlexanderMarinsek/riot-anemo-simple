APPLICATION = anemo_simple

BOARD ?= rhomb-zero

RIOTBASE ?= $(CURDIR)/../..

#USEMODULE += ina220
#USEMODULE += bme280

USEMODULE += xtimer
FEATURES_REQUIRED += periph_timer
FEATURES_REQUIRED += periph_adc
FEATURES_REQUIRED += periph_uart

DIRS += anemo_davis
USEMODULE += anemo_davis

#DIRS += measurement
#USEMODULE += measurement

DIRS += wind_data
USEMODULE += wind_data

#DIRS += el_data
#USEMODULE += el_data

#DIRS += env_data
#USEMODULE += env_data

DIRS += serial_data
USEMODULE += serial_data

DIRS += tasks
USEMODULE += tasks


#CFLAGS += -DTIMER_SPEED=$(TIMER_SPEED)
include $(RIOTBASE)/Makefile.include
