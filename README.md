# RIOT-Anemo-Simple

## Anemo_simple
 Application intended for testing the capabilities of the RIOT-OS embedded
operating system and the connected modules. It cyclically shedules tasks,
where each task is connected to a modlule representing a peripheral measuring
device.

## Configuration
 The use of different modules can be configured in "sys_config.h" by changing
the value of "SYS_CONFING". Correspinding to the system's configuration,
"DATA_FORMATER" must also be changed accordingly.
Another parameter, which can be adjuste is the time period in between
consective measurements, which can be set using "DATA_SEND_PERIOD_MIN"

## Modifications
*At this stage, in order to run, two modifications to RIOT-OS need to be made:*

1. Add "riot-rhomb-zero" board to RIOT-OS, found at:
	https://bitbucket.org/AlexanderMarinsek/riot-rhomb-zero/src/master/

2. Alter "cpu/samd21/periph/timer.c" as follows:

2.1. In "timer_init()", to choose normal frequency operation:
	//TIMER_1_DEV.CTRLA.bit.WAVEGEN = TC_CTRLA_WAVEGEN_NFRQ_Val
	TIMER_1_DEV.CTRLA.bit.WAVEGEN = TC_CTRLA_WAVEGEN_MFRQ_Val;

2.2. In "TIMER_1_ISR()", to not disable match/capture interrupt:
	//TIMER_1_DEV.INTENCLR.reg = TC_INTENCLR_MC0;
