
#include "../../anemo_simple/measurement/measurement.h"

#include <stdio.h>


static Measurement measurement;


void measurement_update_wind_data (int wind_speed, int wind_gust_speed,
		int wind_direction, int wind_gust_peak)
{
	measurement.wind_speed = wind_speed;
	measurement.wind_gust_speed = wind_gust_speed;
	measurement.wind_direction = wind_direction;
	measurement.wind_gust_peak = wind_gust_peak;
	return;
}


void measurement_update_env_data (int air_temperature, int air_RH,
		int air_pressure)
{
	measurement.air_temperature = air_temperature;
	measurement.air_RH = air_RH;
	measurement.air_pressure = air_pressure;
	return;
}


void measurement_update_el_data (int voltage_1, int voltage_2,
		int current_1, int current_2)
{
	measurement.voltage_1 = voltage_1;
	measurement.voltage_2 = voltage_2;
	measurement.current_1 = current_1;
	measurement.current_2 = current_2;
	return;
}


void measurement_send (void)
{
	char buffer[256];
	sprintf(buffer, MEASUREMENT_STRING,
			measurement.wind_speed,
			measurement.wind_gust_speed,
			measurement.wind_direction,
			measurement.wind_gust_peak,
			measurement.air_temperature,
			measurement.air_RH,
			measurement.air_pressure,
			measurement.voltage_1,
			measurement.voltage_2,
			measurement.current_1,
			measurement.current_2);
	printf("%s\n", buffer);
	return;
}

