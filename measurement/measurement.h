
#ifndef MEASUREMENT_H
#define MEASUREMENT_H

#include <stdint.h>

#define MEASUREMENT_STRING	"%d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d"

/*
typedef struct {
	uint16_t wind_speed;
	uint16_t wind_gust_speed;
	uint16_t wind_direction;
	uint8_t wind_gust_peak;
	int16_t air_temperature;
	uint16_t air_RH;
	uint32_t air_pressure;
	int16_t voltage_1;
	int16_t voltag_2;
	int16_t current_1;
	int16_t current_2;
} Measurement;
*/

typedef struct {
	int wind_speed;
	int wind_gust_speed;
	int wind_direction;
	int wind_gust_peak;
	int air_temperature;
	int air_RH;
	int air_pressure;
	int voltage_1;
	int voltage_2;
	int current_1;
	int current_2;
} Measurement;


void measurement_update_wind_data (int wind_speed, int wind_gust_speed,
		int wind_direction, int wind_gust_peak);
void measurement_update_env_data (int air_temperature, int air_RH,
		int air_pressure);
void measurement_update_el_data (int voltage_1, int voltage_2,
		int current_1, int current_2);
void measurement_send (void);

#endif
